﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ToniConcatel.Models;

namespace ToniConcatel.Tests.Controllers
{

    [TestClass]
    public class LogsTest
    {
        [TestMethod]
        public void TestMethodLogsDebug()
        {
            try
            {
                LogsClass.Debug("Test Debug");
            }
            catch (Exception varError)
            {
                LogsClass.Error(varError.Message);
            }
        }

        [TestMethod]
        public void TestMethodLogsInfo()
        {
            try
            {
                LogsClass.Info("Test Info");
            }
            catch (Exception varError)
            {
                LogsClass.Error(varError.Message);
            }
        }

        [TestMethod]
        public void TestMethodLogsWarn()
        {
            try
            {
                LogsClass.Warn("Test Warn");
            }
            catch (Exception varError)
            {
                LogsClass.Error(varError.Message);
            }
        }

        [TestMethod]
        public void TestMethodLogsError()
        {
            try
            {
                LogsClass.Error("Test Error");
            }
            catch (Exception varError)
            {
                LogsClass.Error(varError.Message);
            }
        }

        [TestMethod]
        public void TestMethodLogsFatal()
        {
            try
            {
                LogsClass.Fatal("Test Fatal");
            }
            catch (Exception varError)
            {
                LogsClass.Error(varError.Message);
            }
        }        
    }
}
