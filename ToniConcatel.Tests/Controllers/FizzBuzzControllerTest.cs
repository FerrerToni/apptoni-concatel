﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ToniConcatel.Controllers;
using System.Collections.Generic;
using System.Threading.Tasks;
using ToniConcatel.Models;

namespace ToniConcatel.Tests.Controllers
{
    [TestClass]
    public class FizzBuzzControllerTest
    {
        [TestMethod]
        public void Test1FizzBuzzController()
        {
            try
            {
                FizzBuzzController controller = new FizzBuzzController();

                Task<List<string>> result = controller.Get(2);

                Assert.AreEqual("2\n", result.Result[0]);
                Assert.AreEqual("Fizzn", result.Result[20]);
            }
            catch (Exception varError)
            {
                LogsClass.Error(varError.Message);
            }            
        }


        [TestMethod]
        public void Test2FizzBuzzController()
        {
            try
            {
                FizzBuzzController controller = new FizzBuzzController();

                Task<List<string>> result = controller.Get(2);
                Task<List<string>> test = controller.Get(500);

                Assert.AreEqual(test.Result[6], result.Result[13]);
            }
            catch (Exception varError)
            {
                LogsClass.Error(varError.Message);
            }
        }
    }
}
