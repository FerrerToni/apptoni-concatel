﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using ToniConcatel.Models;

namespace ToniConcatel.Controllers
{
    public class FizzBuzzController : ApiController
    {
        public async Task<List<string>> Get(int startNumber)
        {            
            try
            {                
                return await CalculateFizzBuzzClass.FncCalculateFizzBuzz(startNumber);
            }
            catch (Exception varError)
            {
                List<string> chainFizzBuzz = new List<string>();
                LogsClass.Error(varError.Message);
                return chainFizzBuzz;
            }

        }        
    }
}