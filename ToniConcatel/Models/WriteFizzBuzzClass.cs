﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace ToniConcatel.Models
{
    public class WriteFizzBuzzClass
    {

        public static async Task FncWriteTextAsync(string filePath, List<string> chainFizzBuzz)
        {
            try
            {
                LogsClass.Info("Start Function Write In File FizzBuzz");
                foreach (string line in chainFizzBuzz)
                {
                    byte[] encodedText = System.Text.Encoding.Unicode.GetBytes(line);

                    using (FileStream sourceStream = new FileStream(filePath, FileMode.Append, FileAccess.Write, FileShare.None, bufferSize: 4096, useAsync: true))
                    {
                        await sourceStream.WriteAsync(encodedText, 0, encodedText.Length);
                    };
                }
                LogsClass.Info("End Function Write In File FizzBuzz");
            }
            catch (System.IO.DirectoryNotFoundException vardirectoryNotFoundEx)
            {
                LogsClass.Warn(vardirectoryNotFoundEx.Message);
            }
            catch (Exception varError)
            {
                LogsClass.Error(varError.Message);
            }
        }
    }
}