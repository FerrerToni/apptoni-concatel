﻿using System;

namespace ToniConcatel.Models
{
    public class CustomNullExceptionClass : Exception
    {
        public CustomNullExceptionClass()
        {
            LogsClass.Error("Null exception");
        }

        public CustomNullExceptionClass (string message) : base(message)
        {
            LogsClass.Error(message);
        }

    }
}