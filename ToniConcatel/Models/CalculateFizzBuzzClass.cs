﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace ToniConcatel.Models
{
    public class CalculateFizzBuzzClass
    {
        public static async Task<List<string>> FncCalculateFizzBuzz(int startNumber)
        {
            try
            {
                LogsClass.Info("Start Function Calculate FizzBuzz");
                List<string> chainFizzBuzz = new List<string>();
                int numbersSeries = int.Parse(WebConfigurationManager.AppSettings["NumerosPorSerie"]); //20 Para pruebas unitarias
               

                for (int i = startNumber; i < (startNumber + numbersSeries); i++)
                {
                    if (i % 5 == 0 && i % 3 == 0)
                    {
                        chainFizzBuzz.Add("FizzBuzz\n");
                    }
                    else if (i % 5 == 0)
                    {
                        chainFizzBuzz.Add("Buzz\n");
                    }
                    else if (i % 3 == 0)
                    {
                        chainFizzBuzz.Add("Fizz\n");
                    }
                    else
                    {
                        chainFizzBuzz.Add(i.ToString() + "\n");
                    }
                }

                chainFizzBuzz.Add(DateTime.Now.ToString() + "\n\n");

                if (chainFizzBuzz == null)
                {
                    throw new CustomNullExceptionClass("Calculate FizzBuzz class returns null");
                }

                await WriteFizzBuzzClass.FncWriteTextAsync(WebConfigurationManager.AppSettings["VarPathFileFizzBuzz"], chainFizzBuzz);
                LogsClass.Info("End Function Calculate FizzBuzz");
                return chainFizzBuzz;
            }
            catch (Exception varError)
            {
                List<string> cadenaFizzBuzz = new List<string>();
                LogsClass.Error(varError.Message);
                return cadenaFizzBuzz;
            }
        }        
    }
}